// IMPORT MODULE-LIBRARY
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule, Http, XHRBackend, RequestOptions, BaseRequestOptions } from '@angular/http';
import { httpFactory } from '../app/shared/services/httpFactory';
import { Router } from '@angular/router';

// IMPORT ROUTING MODULE
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

// IMPORT MODULE
import { LayoutModule } from './layout/layout.module';
import { HomeModule } from './home/home.module';
import { LoginModule } from './login/login.module';
import { CarDriverModule } from './car-driver/car-driver.module';
import { MechanicianModule } from './mechanician/mechanician.module';
import { SignupMechanicianModule } from './signup/signup-mechanician/signup-mechanician.module';
import { SignupCarDriverModule } from './signup/signup-car-driver/signup-car-driver.module';

// IMPORT COMPONENT
import { AppComponent } from './app.component';

// IMPORT GUARD
import { AuthGuard } from './shared/guard';

// IMPORT SERVICE
import { SignupCarDriverWorkflowService } from './shared/services/car-driver/signup-car-driver-workflow.service';
import { SignupCarDriverService } from './shared/services/car-driver/signup-car-driver.service';
import { MechanicianSearchService } from './shared/services/mechanician/mechanician-search.service';
import { AdService } from './shared/services/car-driver/ad.service';
import { SignupMechanicianService } from './shared/services/mechanician/signup-mechanician.service';
import { SignupMechanicianWorkflowService } from './shared/services/mechanician/signup-mechanician-workflow.service';
import { AuthenticationService } from './shared/services/authentication.service';
import { CarService } from './shared/services/car-driver/car.service';
import { ClientSearchService } from './shared/services/car-driver/client-search.service';
import { QuoteService } from './shared/services/mechanician/quote.service';
import { AlertService } from './shared/services/alert.service';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    LayoutModule,
    HomeModule,
    LoginModule,
    CarDriverModule,
    MechanicianModule,
    SignupMechanicianModule,
    SignupCarDriverModule,
    SweetAlert2Module.forRoot({
      confirmButtonColor: '#000000'
    })
  ],
  schemas: [NO_ERRORS_SCHEMA],
  declarations: [AppComponent],
  providers: [
    AuthGuard,
    { provide: Http, useFactory: httpFactory, deps: [XHRBackend, RequestOptions, Router] },
    { provide: SignupMechanicianService, useClass: SignupMechanicianService },
    { provide: SignupMechanicianWorkflowService, useClass: SignupMechanicianWorkflowService },
    { provide: SignupCarDriverWorkflowService, useClass: SignupCarDriverWorkflowService },
    { provide: SignupCarDriverService, useClass: SignupCarDriverService },
    MechanicianSearchService,
    AdService,
    AuthenticationService,
    BaseRequestOptions,
    CarService,
    ClientSearchService,
    QuoteService,
    AlertService
  ],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }
