import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarDriverComponent } from './car-driver/car-driver.component';
import { CarDriverHomeComponent } from './car-driver/car-driver-home/car-driver-home.component';
import { CarDriverDashboardComponent } from './car-driver/car-driver-dashboard/car-driver-dashboard.component';
import { CarDriverMyGarageComponent } from './car-driver/car-driver-my-garage/car-driver-my-garage.component';
import { CarDriverHistoricComponent } from './car-driver/car-driver-historic/car-driver-historic.component';
import { CarDriverQuoteComponent } from './car-driver/car-driver-quote/car-driver-quote.component';
import { AuthGuard } from '../shared/guard';

const routes: Routes = [
    {
        path: 'car-driver/home',
        component: CarDriverComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: CarDriverHomeComponent, outlet: 'car-driver-view' },
        ]
    },
    {
        path: 'car-driver/quote',
        component: CarDriverComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: CarDriverQuoteComponent, outlet: 'car-driver-view' },
        ]
    },
    {
        path: 'car-driver/dashboard',
        component: CarDriverComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: CarDriverDashboardComponent, outlet: 'car-driver-view' },
        ]
    },
    {
        path: 'car-driver/my-garage',
        component: CarDriverComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: CarDriverMyGarageComponent, outlet: 'car-driver-view' },
        ]
    },
    {
        path: 'car-driver/historic',
        component: CarDriverComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: CarDriverHistoricComponent, outlet: 'car-driver-view' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CarDriverRoutingModule {}
