// IMPORT MODULE
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';

// IMPORT ROUTING
import { CarDriverRoutingModule } from './car-driver-routing.module';

// IMPORT COMPONENT
import { CarDriverComponent } from './car-driver/car-driver.component';
import { CarDriverHomeComponent } from './car-driver/car-driver-home/car-driver-home.component';
import { CarDriverDashboardComponent } from './car-driver/car-driver-dashboard/car-driver-dashboard.component';
import { CarDriverMyGarageComponent } from './car-driver/car-driver-my-garage/car-driver-my-garage.component';
import { CarDriverHistoricComponent } from './car-driver/car-driver-historic/car-driver-historic.component';
import { CarDriverQuoteComponent } from './car-driver/car-driver-quote/car-driver-quote.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    CarDriverRoutingModule
  ],
  declarations: [
    CarDriverComponent,
    CarDriverHomeComponent,
    CarDriverDashboardComponent,
    CarDriverMyGarageComponent,
    CarDriverHistoricComponent,
    CarDriverQuoteComponent,
  ]
})
export class CarDriverModule { }
