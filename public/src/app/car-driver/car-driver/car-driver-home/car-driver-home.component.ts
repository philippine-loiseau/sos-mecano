import { Component, OnInit } from '@angular/core';
import { MechanicianSearchService } from '../../../shared/services/mechanician/mechanician-search.service';

@Component({
  selector: 'app-car-driver-home',
  templateUrl: './car-driver-home.component.html',
  styleUrls: ['./car-driver-home.component.scss'],
  providers: [MechanicianSearchService]
})
export class CarDriverHomeComponent implements OnInit {

  constructor() { }

  ngOnInit() { }

}
