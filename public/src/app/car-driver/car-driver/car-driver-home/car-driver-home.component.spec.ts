import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarDriverHomeComponent } from './car-driver-home.component';

describe('CarDriverHomeComponent', () => {
  let component: CarDriverHomeComponent;
  let fixture: ComponentFixture<CarDriverHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarDriverHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarDriverHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
