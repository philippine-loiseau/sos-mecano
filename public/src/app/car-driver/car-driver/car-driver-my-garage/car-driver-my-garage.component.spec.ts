import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarDriverMyGarageComponent } from './car-driver-my-garage.component';

describe('CarDriverMyGarageComponent', () => {
  let component: CarDriverMyGarageComponent;
  let fixture: ComponentFixture<CarDriverMyGarageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarDriverMyGarageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarDriverMyGarageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
