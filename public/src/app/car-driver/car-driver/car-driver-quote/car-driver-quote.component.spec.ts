import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarDriverQuoteComponent } from './car-driver-quote.component';

describe('CarDriverQuoteComponent', () => {
  let component: CarDriverQuoteComponent;
  let fixture: ComponentFixture<CarDriverQuoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarDriverQuoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarDriverQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
