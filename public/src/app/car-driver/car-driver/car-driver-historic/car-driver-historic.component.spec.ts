import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarDriverHistoricComponent } from './car-driver-historic.component';

describe('CarDriverHistoricComponent', () => {
  let component: CarDriverHistoricComponent;
  let fixture: ComponentFixture<CarDriverHistoricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarDriverHistoricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarDriverHistoricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
