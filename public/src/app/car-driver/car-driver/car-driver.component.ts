import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-car-driver',
  templateUrl: './car-driver.component.html',
  styleUrls: ['./car-driver.component.scss']
})
export class CarDriverComponent implements OnInit {

  routerHome = '/car-driver/home';
  routerQuote = '/car-driver/quote';
  routerDashboard = '/car-driver/dashboard';
  routerMyGarage = '/car-driver/my-garage';
  routerHistoric = '/car-driver/historic';

  home = 'ACCUEIL';
  quote = 'DEMANDE DE DEVIS';
  dashboard = 'MON TABLEAU DE BORD';
  mygarage = 'MES GARAGES';
  historic = 'HISTORIQUES';

  constructor() { }

  ngOnInit() {
  }

}
