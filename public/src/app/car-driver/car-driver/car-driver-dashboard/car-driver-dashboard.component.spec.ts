import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarDriverDashboardComponent } from './car-driver-dashboard.component';

describe('CarDriverDashboardComponent', () => {
  let component: CarDriverDashboardComponent;
  let fixture: ComponentFixture<CarDriverDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarDriverDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarDriverDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
