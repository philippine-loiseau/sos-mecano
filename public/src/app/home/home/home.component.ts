import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { RegisterCarDriverComponent } from '../../layout/register-car-driver/register-car-driver.component';
import { RegisterMechanicianComponent } from '../../layout/register-mechanician/register-mechanician.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService) { }

  ngOnInit() {
  }

  openRegisterCarDriver() {
    const initialState = {
      title: 'CHER AUTOMOBILISTE...'
    };
    this.bsModalRef = this.modalService.show(RegisterCarDriverComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
  }

  openRegisterMechanician() {
    const initialState = {
      title: 'CHER MECANICIEN...'
    };
    this.bsModalRef = this.modalService.show(RegisterMechanicianComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
  }
}
