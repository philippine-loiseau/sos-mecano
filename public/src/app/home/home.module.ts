import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { LayoutModule } from '../layout/layout.module';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: 'home', component: HomeComponent,
  }];

@NgModule({
  imports: [CommonModule, LayoutModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [HomeComponent]
})
export class HomeModule {}
