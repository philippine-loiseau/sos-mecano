import { NgModule } from '@angular/core';
import { Routes, RouterModule, RouterLink } from '@angular/router';
import { AppComponent } from './app.component';

// IMPORT ROUTING MODULE
import { LoginRoutingModule } from './login/login-routing.module';
import { CarDriverRoutingModule } from './car-driver/car-driver-routing.module';
import { MechanicianRoutingModule } from './mechanician/mechanician-routing.module';
import { SignupMechanicianRoutingModule } from './signup/signup-mechanician/signup-mechanician-routing.module';
import { SignupCarDriverRoutingModule } from './signup/signup-car-driver/signup-car-driver-routing.module';

const routes: Routes = [{ path: '**', redirectTo: 'home' }];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    LoginRoutingModule,
    CarDriverRoutingModule,
    MechanicianRoutingModule,
    SignupMechanicianRoutingModule,
    SignupCarDriverRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
