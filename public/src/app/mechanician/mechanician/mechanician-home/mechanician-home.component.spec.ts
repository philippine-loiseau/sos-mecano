import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MechanicianHomeComponent } from './mechanician-home.component';

describe('MechanicianHomeComponent', () => {
  let component: MechanicianHomeComponent;
  let fixture: ComponentFixture<MechanicianHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MechanicianHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MechanicianHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
