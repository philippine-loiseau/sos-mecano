import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MechanicianComponent } from './mechanician.component';

describe('MechanicianComponent', () => {
  let component: MechanicianComponent;
  let fixture: ComponentFixture<MechanicianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MechanicianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MechanicianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
