import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MechanicianEmergencyServiceComponent } from './mechanician-emergency-service.component';

describe('MechanicianEmergencyServiceComponent', () => {
  let component: MechanicianEmergencyServiceComponent;
  let fixture: ComponentFixture<MechanicianEmergencyServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MechanicianEmergencyServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MechanicianEmergencyServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
