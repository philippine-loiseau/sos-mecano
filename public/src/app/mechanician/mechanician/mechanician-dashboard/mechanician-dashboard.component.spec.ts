import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MechanicianDashboardComponent } from './mechanician-dashboard.component';

describe('MechanicianDashboardComponent', () => {
  let component: MechanicianDashboardComponent;
  let fixture: ComponentFixture<MechanicianDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MechanicianDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MechanicianDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
