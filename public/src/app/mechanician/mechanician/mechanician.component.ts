import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mechanician',
  templateUrl: './mechanician.component.html',
  styleUrls: ['./mechanician.component.scss']
})
export class MechanicianComponent implements OnInit {

  routerHome = '/mechanician/home';
  routerEmergencyService = '/mechanician/emergency-service';
  routerDashboard = '/mechanician/dashboard';
  routerMyClient = '/mechanician/client';
  routerHistoric = '/mechanician/historic';

  home = 'ACCUEIL';
  emergencyService = 'SERVICE DE DEPANNAGE';
  dashboard = 'MON TABLEAU DE BORD';
  myClient = 'MES CLIENTS';
  historic = 'HISTORIQUES';

  constructor() { }

  ngOnInit() {
  }

}
