import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MechanicianClientComponent } from './mechanician-client.component';

describe('MechanicianClientComponent', () => {
  let component: MechanicianClientComponent;
  let fixture: ComponentFixture<MechanicianClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MechanicianClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MechanicianClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
