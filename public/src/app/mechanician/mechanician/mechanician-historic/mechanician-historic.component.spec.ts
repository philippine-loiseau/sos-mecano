import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MechanicianHistoricComponent } from './mechanician-historic.component';

describe('MechanicianHistoricComponent', () => {
  let component: MechanicianHistoricComponent;
  let fixture: ComponentFixture<MechanicianHistoricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MechanicianHistoricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MechanicianHistoricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
