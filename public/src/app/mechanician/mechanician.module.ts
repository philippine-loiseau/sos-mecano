// IMPORT MODULE
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../layout/layout.module';

// IMPORT ROUTING MODULE
import { MechanicianRoutingModule } from './mechanician-routing.module';

// IMPORT COMPONENT
import { MechanicianHomeComponent } from './mechanician/mechanician-home/mechanician-home.component';
import { MechanicianDashboardComponent } from './mechanician/mechanician-dashboard/mechanician-dashboard.component';
import { MechanicianClientComponent } from './mechanician/mechanician-client/mechanician-client.component';
import { MechanicianHistoricComponent } from './mechanician/mechanician-historic/mechanician-historic.component';
import { MechanicianComponent } from './mechanician/mechanician.component';
import { MechanicianEmergencyServiceComponent } from './mechanician/mechanician-emergency-service/mechanician-emergency-service.component';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    MechanicianRoutingModule
  ],
  declarations: [
    MechanicianComponent,
    MechanicianHomeComponent,
    MechanicianDashboardComponent,
    MechanicianClientComponent,
    MechanicianHistoricComponent,
    MechanicianEmergencyServiceComponent
  ]
})
export class MechanicianModule { }
