// IMPORT MODULE
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// IMPORT COMPONENT
import { MechanicianComponent } from './mechanician/mechanician.component';
import { MechanicianHomeComponent } from './mechanician/mechanician-home/mechanician-home.component';
import { MechanicianDashboardComponent } from './mechanician/mechanician-dashboard/mechanician-dashboard.component';
import { MechanicianClientComponent } from './mechanician/mechanician-client/mechanician-client.component';
import { MechanicianHistoricComponent } from './mechanician/mechanician-historic/mechanician-historic.component';
import { MechanicianEmergencyServiceComponent } from './mechanician/mechanician-emergency-service/mechanician-emergency-service.component';
import { AuthGuard } from '../shared/guard';

const routes: Routes = [
    {
        path: 'mechanician/home',
        component: MechanicianComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: MechanicianHomeComponent, outlet: 'mechanician-view' },
        ]
    },
    {
        path: 'mechanician/emergency-service',
        component: MechanicianComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: MechanicianEmergencyServiceComponent, outlet: 'mechanician-view' },
        ]
    },
    {
        path: 'mechanician/dashboard',
        component: MechanicianComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: MechanicianDashboardComponent, outlet: 'mechanician-view' },
        ]
    },
    {
        path: 'mechanician/client',
        component: MechanicianComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: MechanicianClientComponent, outlet: 'mechanician-view' },
        ]
    },
    {
        path: 'mechanician/historic',
        component: MechanicianComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', component: MechanicianHistoricComponent, outlet: 'mechanician-view' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MechanicianRoutingModule { }
