import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdCheckComponent } from './ad-check.component';

describe('QuoteCheckComponent', () => {
  let component: AdCheckComponent;
  let fixture: ComponentFixture<AdCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
