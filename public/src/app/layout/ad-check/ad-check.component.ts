import { Component, OnInit, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AD } from '../../shared/models/car-driver/ad.model';
import { AdService } from '../../shared/services/car-driver/ad.service';
import { QuoteDetailsComponent } from '../quote-details/quote-details.component';
import * as moment from 'moment';

@Component({
  selector: 'app-ad-check',
  templateUrl: './ad-check.component.html',
  styleUrls: ['./ad-check.component.scss']
})
export class AdCheckComponent implements OnInit {

  title: string;
  closeBtnName: string;

  @Input() ads: AD[] = [];

  constructor(public bsModalRef: BsModalRef, private adService: AdService, private modalService: BsModalService) { }

  ngOnInit() {
    this.getAdsWithQuotes();
  }

  getAdsWithQuotes() {
    this.adService.getAdsWithQuotesByUser().subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.ads = nodeResponse.data;
        for (let i = 0; i < this.ads.length; i++) {
          this.ads[i].date_ad = moment(this.ads[i].date_ad).locale('fr').format('LL');
        }
      }
    });
  }

  openQuoteView(ad: AD) {
    this.bsModalRef.hide();
    const initialState = {
      title: 'CHOISSEZ VOTRE DEVIS',
      ad: ad
    };
    this.bsModalRef = this.modalService.show(QuoteDetailsComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
  }


}
