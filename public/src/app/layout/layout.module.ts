// IMPORT MODULE
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


// IMPORT COMPONENT
import { ImagePanelComponent } from './image-panel/image-panel.component';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { SignupNavbarComponent } from './signup-navbar/signup-navbar.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { TabsComponent } from './tabs/tabs.component';
import { ProfileCarDriverComponent } from './profile-car-driver/profile-car-driver.component';
import { SignupLoginComponent } from './signup-login/signup-login.component';
import { ProfileMechanicianComponent } from './profile-mechanician/profile-mechanician.component';
import { TabsChildComponent } from './tabs-child/tabs-child.component';
import { HomeHeaderComponent } from './home-header/home-header.component';
import { RegisterCarDriverComponent } from './register-car-driver/register-car-driver.component';
import { RegisterMechanicianComponent } from './register-mechanician/register-mechanician.component';
import { AdAddComponent } from './ad-add/ad-add.component';
import { AdCheckComponent } from './ad-check/ad-check.component';
import { AdInfoComponent } from './ad-info/ad-info.component';
import { AdViewComponent } from './ad-view/ad-view.component';
import { AdDetailsComponent } from './ad-details/ad-details.component';
import { SearchClientComponent } from './search-client/search-client.component';
import { QuoteInfoComponent } from './quote-info/quote-info.component';
import { QuoteViewComponent } from './quote-view/quote-view.component';
import { QuoteAcceptComponent } from './quote-accept/quote-accept.component';
import { QuoteRefusedComponent } from './quote-refused/quote-refused.component';
import { QuoteAddComponent } from './quote-add/quote-add.component';
import { LoadingModule } from 'ngx-loading';
import { QuoteDetailsComponent } from './quote-details/quote-details.component';
import { CarDetailsComponent } from './car-details/car-details.component';
import { AdUpdateComponent } from './ad-update/ad-update.component';
import { DetailsQuoteComponent } from './details-quote/details-quote.component';
import { DetailsAdComponent } from './details-ad/details-ad.component';

@NgModule({
  imports: [CommonModule,
    RouterModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    LoadingModule
  ],
  declarations: [
    ImagePanelComponent,
    FooterComponent,
    SidebarComponent,
    HeaderComponent,
    SignupNavbarComponent,
    SearchBarComponent,
    TabsComponent,
    ProfileCarDriverComponent,
    SignupLoginComponent,
    ProfileMechanicianComponent,
    TabsChildComponent,
    HomeHeaderComponent,
    RegisterCarDriverComponent,
    RegisterMechanicianComponent,
    AdAddComponent,
    AdCheckComponent,
    AdInfoComponent,
    AdViewComponent,
    AdDetailsComponent,
    SearchClientComponent,
    QuoteInfoComponent,
    QuoteViewComponent,
    QuoteAcceptComponent,
    QuoteRefusedComponent,
    QuoteAddComponent,
    QuoteDetailsComponent,
    CarDetailsComponent,
    AdUpdateComponent,
    DetailsQuoteComponent,
    DetailsAdComponent,
  ],
  exports: [
    ImagePanelComponent,
    FooterComponent,
    SidebarComponent,
    HeaderComponent,
    SignupNavbarComponent,
    SearchBarComponent,
    TabsComponent,
    ProfileCarDriverComponent,
    ProfileMechanicianComponent,
    SignupLoginComponent,
    TabsChildComponent,
    HomeHeaderComponent,
    RegisterCarDriverComponent,
    RegisterMechanicianComponent,
    AdAddComponent,
    AdCheckComponent,
    AdInfoComponent,
    AdViewComponent,
    AdDetailsComponent,
    SearchClientComponent,
    QuoteInfoComponent,
    QuoteViewComponent,
    QuoteAcceptComponent,
    QuoteRefusedComponent,
    QuoteAddComponent,
    QuoteDetailsComponent,
    CarDetailsComponent,
    AdUpdateComponent,
    DetailsQuoteComponent,
    DetailsAdComponent
  ],
  bootstrap: [
    RegisterCarDriverComponent,
    AdAddComponent,
    AdCheckComponent,
    AdViewComponent,
    RegisterMechanicianComponent,
    AdDetailsComponent,
    QuoteViewComponent,
    QuoteAcceptComponent,
    QuoteRefusedComponent,
    QuoteAddComponent,
    QuoteDetailsComponent,
    CarDetailsComponent,
    AdUpdateComponent,
    DetailsQuoteComponent,
    DetailsAdComponent
  ]
})
export class LayoutModule { }
