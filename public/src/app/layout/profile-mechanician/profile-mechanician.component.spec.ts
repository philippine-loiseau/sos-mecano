import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileMechanicianComponent } from './profile-mechanician.component';

describe('ProfileMechanicianComponent', () => {
  let component: ProfileMechanicianComponent;
  let fixture: ComponentFixture<ProfileMechanicianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileMechanicianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileMechanicianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
