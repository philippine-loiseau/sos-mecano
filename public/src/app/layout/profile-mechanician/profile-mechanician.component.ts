import { Component, OnInit } from '@angular/core';
import { Mechanician } from '../../shared/models/mechanician/mechanician.model';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-mechanician',
  templateUrl: './profile-mechanician.component.html',
  styleUrls: ['./profile-mechanician.component.scss']
})
export class ProfileMechanicianComponent implements OnInit {

  mechanician: Mechanician = new Mechanician;

  constructor(public router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.currentUser();
  }


  currentUser() {
    this.authenticationService.getCurrentUser().subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.mechanician = nodeResponse.data;
      } else {
        this.router.navigate(['/login']);      }
    });
  }

}
