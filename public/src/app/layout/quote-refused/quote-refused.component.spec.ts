import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteRefusedComponent } from './quote-refused.component';

describe('QuoteRefusedComponent', () => {
  let component: QuoteRefusedComponent;
  let fixture: ComponentFixture<QuoteRefusedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuoteRefusedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteRefusedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
