import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { QuoteService } from '../../shared/services/mechanician/quote.service';
import * as moment from 'moment';
import { AlertService } from '../../shared/services/alert.service';
import { AD } from '../../shared/models/car-driver/ad.model';
import { Quote } from '../../shared/models/mechanician/quote.model';
import { DetailsQuoteComponent } from '../details-quote/details-quote.component';
import { CarDetailsComponent } from '../car-details/car-details.component';

@Component({
  selector: 'app-quote-refused',
  templateUrl: './quote-refused.component.html',
  styleUrls: ['./quote-refused.component.scss']
})
export class QuoteRefusedComponent implements OnInit {


  title: string;
  closeBtnName: string;

  @Input() quotes = [];

  constructor(public bsModalRef: BsModalRef, 
    private quoteService: QuoteService, 
    private alertService: AlertService,
    private modalService: BsModalService) { }

  ngOnInit() {
    this.quotes = [];
    this.getQuoteRefused();
  }

  getQuoteRefused() {
    this.quoteService.getQuoteRefused().subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.quotes = nodeResponse.data;
        for (let i = 0; i < this.quotes.length; i++) {
          this.quotes[i].Ad.date_ad = moment(this.quotes[i].Ad.date_ad).locale('fr').format('LL');
        }
        console.log(this.quotes);
      } else {
        this.alertService.showAlertErrorServer();
      }
    });
  }

  openQuoteDetails(ad: AD) {
    this.quoteService.getValidatedQuoteByAd(ad.id_ad).subscribe(nodeResponse => {
      const quote = new  Quote();
      quote.accept_quote = true;
      const initialState = {
        title: 'DETAILS DU DEVIS',
        ad: ad,
        quote: quote
      };
      this.bsModalRef = this.modalService.show(DetailsQuoteComponent, { initialState });
      this.bsModalRef.content.closeBtnName = 'FERMER';
    });
  }

  openCarDetails(ad: AD) {
      const initialState = {
        title: 'DETAILS DE LA VOITURE',
        ad: ad
      };
      this.bsModalRef = this.modalService.show(CarDetailsComponent, { initialState });
      this.bsModalRef.content.closeBtnName = 'FERMER';
  }

}
