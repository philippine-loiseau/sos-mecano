import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { MechanicianSearchService } from '../../shared/services/mechanician/mechanician-search.service';
import { NodeResponse } from '../../shared/models/node_response';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {


  results: Object;
  searchTerm$ = new Subject<string>();

  constructor(private mechanicianSearchService: MechanicianSearchService) {

    this.mechanicianSearchService.search(this.searchTerm$)
      .debounceTime(500)
      .subscribe(response => {
        const nodeResponse: NodeResponse = JSON.parse(response);
        this.results = nodeResponse.data;
      });
  }
  ngOnInit() { }
}
