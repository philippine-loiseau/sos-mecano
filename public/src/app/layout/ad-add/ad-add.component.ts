import { Component, OnInit, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AdService } from '../../shared/services/car-driver/ad.service';
import { AD } from '../../shared/models/car-driver/ad.model';
import { CarService } from '../../shared/services/car-driver/car.service';
import { CarDriver } from '../../shared/models/car-driver/car-driver.model';
import { AlertService } from '../../shared/services/alert.service';

@Component({
  selector: 'app-ad-add',
  templateUrl: './ad-add.component.html',
  styleUrls: ['./ad-add.component.scss']
})
export class AdAddComponent implements OnInit {

  title: string;
  closeBtnName: string;
  form: any;

  ad: AD;
  cars: CarDriver[] = [];

  constructor(
    private adService: AdService,
    private carService: CarService,
    public bsModalRef: BsModalRef,
    private alertService: AlertService) {
  }

  ngOnInit() {
    this.getUserCar();
    this.ad = this.adService.getAd();
    this.ad.ad_type = 'Réparation';
  }
  onPhotoAdChange(event) {
    this.fileChanged(event, 'photo_ad');
  }

  fileChanged(event, fileName) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if ('photo_ad' === fileName) {
          this.ad.photo_ad = reader.result.split(',')[1];
        }
      };
    }
  }

  save(form: any): boolean {
    if (!form) {
      console.log('create', this.ad);
      this.adService.createAd(this.ad)
        .subscribe(nodeResponse => {
          if (nodeResponse.success) {
            this.alertService.showAlertAdAdd();
            this.bsModalRef.hide();
          } else {
            this.alertService.showAlertErroAdAdd();
          }
        });
      return false;
    } else {
      return true;
    }
  }

  getUserCar() {
    this.carService.getUserCar().subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.cars = nodeResponse.data;
        this.ad.id_car = this.cars[0].id_car;
      } else {
        this.alertService.showAlertNotCar();
      }
    });
  }
}
