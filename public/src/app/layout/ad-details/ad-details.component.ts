import { Component, OnInit, Input } from '@angular/core';
import { AD } from '../../shared/models/car-driver/ad.model';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CarDetailsComponent } from '../car-details/car-details.component';
import { AdUpdateComponent } from '../ad-update/ad-update.component';

@Component({
  selector: 'app-ad-details',
  templateUrl: './ad-details.component.html',
  styleUrls: ['./ad-details.component.scss']
})
export class AdDetailsComponent implements OnInit {

  title: string;
  closeBtnName: string;
  updateBtnName: string;

  modalRef: BsModalRef;

  @Input() ad: AD;

  constructor(public bsModalRef: BsModalRef, private modalService: BsModalService,
  ) { }

  ngOnInit() { }

  openCarDetails(ad: AD) {
    const initialState = {
      title: 'DETAILS DE LA VOITURE',
      ad: ad
    };
    this.bsModalRef = this.modalService.show(CarDetailsComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
  }

  openUpdateAd(ad: AD) {
    const initialState = {
      title: 'MODIFIER VOTRE ANNONCE',
      ad: ad
    };
    this.bsModalRef = this.modalService.show(AdUpdateComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
  }


}
