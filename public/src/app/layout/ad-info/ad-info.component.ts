import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { AdAddComponent } from '../ad-add/ad-add.component';
import { AdViewComponent } from '../ad-view/ad-view.component';
import { AdCheckComponent } from '../ad-check/ad-check.component';

@Component({
  selector: 'app-ad-info',
  templateUrl: './ad-info.component.html',
  styleUrls: ['./ad-info.component.less']
})
export class AdInfoComponent implements OnInit {

  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService) { }

  openQuoteAdd() {
    const initialState = {
      title: 'DEMANDE DE DEVIS'
    };
    this.bsModalRef = this.modalService.show(AdAddComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
  }

  openQuoteView() {
    const initialState = {
      title: 'MES DEMANDES EN COURS'
    };
    this.bsModalRef = this.modalService.show(AdViewComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
  }

  openQuoteCheck() {
    const initialState = {
      title: 'MES DEMANDES ACCEPTES'
    };
    this.bsModalRef = this.modalService.show(AdCheckComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';

  }

  ngOnInit() {
  }

}
