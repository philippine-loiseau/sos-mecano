import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CarDriver } from '../../shared/models/car-driver/car-driver.model';
import { CarService } from '../../shared/services/car-driver/car.service';
import { AD } from '../../shared/models/car-driver/ad.model';
import { AlertService } from '../../shared/services/alert.service';
import * as moment from 'moment';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.scss']
})
export class CarDetailsComponent implements OnInit {

  title: string;
  closeBtnName: string;

  @Input() carDetails: CarDriver;
  @Input() ad: AD;

  constructor(public bsModalRef: BsModalRef, private carService: CarService, private alertService: AlertService) { }

  ngOnInit() {
    this.getInfoCar();
    this.carDetails = new CarDriver();
  }

  getInfoCar() {
    this.carService.getCarByAd(this.ad.id_car).subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.carDetails = nodeResponse.data;
        if (this.carDetails) {
          this.carDetails.in_service = moment(this.carDetails.in_service).locale('fr').format('LL');
        }
      } else {
        this.alertService.showAlertErrorServer();
      }
    });
  }

}
