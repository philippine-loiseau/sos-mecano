import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterCarDriverComponent } from './register-car-driver.component';

describe('RegisterCarDriverComponent', () => {
  let component: RegisterCarDriverComponent;
  let fixture: ComponentFixture<RegisterCarDriverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterCarDriverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterCarDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
