import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-register-car-driver',
  templateUrl: './register-car-driver.component.html',
  styleUrls: ['./register-car-driver.component.scss']
})
export class RegisterCarDriverComponent implements OnInit {

  title: string;
  closeBtnName: string;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

}
