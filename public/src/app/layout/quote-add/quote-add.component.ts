import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { QuoteService } from '../../shared/services/mechanician/quote.service';
import { Quote } from '../../shared/models/mechanician/quote.model';
import { AD } from '../../shared/models/car-driver/ad.model';
import * as moment from 'moment';
import { AlertService } from '../../shared/services/alert.service';

@Component({
  selector: 'app-quote-add',
  templateUrl: './quote-add.component.html',
  styleUrls: ['./quote-add.component.scss']
})
export class QuoteAddComponent implements OnInit {

  title: string;
  closeBtnName: string;
  form: any;

  quote: Quote;
  ad: AD;

  constructor(public bsModalRef: BsModalRef, private quoteService: QuoteService, private alertService: AlertService) { }

  ngOnInit() {
    const userId = JSON.parse(localStorage.getItem('token')).user;
    this.quote = this.quoteService.getQuote();
    this.quote.date_quote = moment().toLocaleString();
    this.quote.id_ad = this.ad.id_ad;
    this.quote.id_co = userId;
  }

  save(form: any): boolean {
    if (!form) {
      this.quoteService.createQuote(this.quote)
        .subscribe(nodeResponse => {
          if (nodeResponse.success) {
            this.alertService.showAlertQuoteAdd();
            this.bsModalRef.hide();
          } else {
            this.alertService.showAlertErroAdAdd();
          }
        });
      return false;
    } else {
      return true;
    }
  }

}
