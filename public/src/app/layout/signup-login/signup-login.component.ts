import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { Constants } from '../../shared/models/constants';

@Component({
  selector: 'app-signup-login',
  templateUrl: './signup-login.component.html',
  styleUrls: ['./signup-login.component.scss']
})
export class SignupLoginComponent implements OnInit {

  model: any = {};
  error = '';

  constructor(public router: Router, private authenticationService: AuthenticationService) {
    this.authenticationService.logout();

  }

  ngOnInit() {
  }

  onLoggedin() {
    this.authenticationService.login(this.model.email, this.model.password).subscribe(response => {
      if (response.success) {
        if (response.type === Constants.INFO_USER_CONNECTED) {
          this.router.navigate(['car-driver/home']);
        } else if (response.type === Constants.INFO_COMPANY_CONNECTED) {
          this.router.navigate(['mechanician/home']);
        }
      } else {
        this.error = 'Username or password is incorrect';
      }
    });

  }
}
