import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Quote } from '../../shared/models/mechanician/quote.model';
import { QuoteService } from '../../shared/services/mechanician/quote.service';
import { AD } from '../../shared/models/car-driver/ad.model';
import { AdDetailsComponent } from '../ad-details/ad-details.component';
import { Constants } from '../../shared/models/constants';
import * as moment from 'moment';
import { AlertService } from '../../shared/services/alert.service';
import { AdService } from '../../shared/services/car-driver/ad.service';

@Component({
  selector: 'app-quote-view',
  templateUrl: './quote-view.component.html',
  styleUrls: ['./quote-view.component.scss']
})
export class QuoteViewComponent implements OnInit {


  title: string;
  closeBtnName: string;
  modalRef: BsModalRef;

  @Input() quotes: Quote[] = [];

  constructor(
    public bsModalRef: BsModalRef,
    private quoteService: QuoteService,
    private modalService: BsModalService,
    private alertService: AlertService,
    private adService: AdService
  ) { }

  ngOnInit() {
    this.getWaitingQuotes();
  }

  openAdDetails(idAd: string) {
    this.adService.getAdById(idAd).subscribe(nodeResponse => {
      const initialState = {
        title: 'DETAILS DE LA REPARATION',
        ad: nodeResponse.data
      };
      console.log(initialState);
      this.bsModalRef = this.modalService.show(AdDetailsComponent, { initialState });
      this.bsModalRef.content.closeBtnName = 'FERMER';
    });
  }

  openAdDelete(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  confirm(quote: Quote): void {
    this.modalRef.hide();
    this.quoteService.deleteQuote(quote).subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.quotes = nodeResponse.data;
        this.alertService.showAlertQuoteDelete();
        this.bsModalRef.hide();
      } else if (nodeResponse.type === Constants.INFO_NO_QUOTE_FOUND) {
        alert('Votre annonce n\'a pas été trouvé');
      }
    });
  }

  decline(): void {
    this.modalRef.hide();
  }

  getWaitingQuotes() {
    this.quoteService.getWaitingQuotes().subscribe(nodeResponse => {
      this.quotes = nodeResponse.data;
      for (let i = 0; i < this.quotes.length; i++) {
        this.quotes[i].date_quote = moment(this.quotes[i].date_quote).locale('fr').format('LL');
      }
    });
  }

}
