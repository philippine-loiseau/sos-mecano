import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { AD } from '../../shared/models/car-driver/ad.model';

@Component({
  selector: 'app-details-ad',
  templateUrl: './details-ad.component.html',
  styleUrls: ['./details-ad.component.scss']
})
export class DetailsAdComponent implements OnInit {

  title: string;
  closeBtnName: string;

  @Input() ad: AD;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

}
