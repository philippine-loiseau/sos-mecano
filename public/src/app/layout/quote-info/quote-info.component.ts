import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { QuoteViewComponent } from '../quote-view/quote-view.component';
import { QuoteAcceptComponent } from '../quote-accept/quote-accept.component';
import { QuoteRefusedComponent } from '../quote-refused/quote-refused.component';

@Component({
  selector: 'app-quote-info',
  templateUrl: './quote-info.component.html',
  styleUrls: ['./quote-info.component.less']
})
export class QuoteInfoComponent implements OnInit {

  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService) { }

  ngOnInit() {
  }
  openQuoteView() {
    const initialState = {
      title: 'DEVIS PROPOSES'
    };
    this.bsModalRef = this.modalService.show(QuoteViewComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
  }

  openQuoteAccept() {
    const initialState = {
      title: 'DEVIS ACCEPTES'
    };
    this.bsModalRef = this.modalService.show(QuoteAcceptComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
  }


  openQuoteRefused() {
    const initialState = {
      title: 'DEVIS REFUSES'
    };
    this.bsModalRef = this.modalService.show(QuoteRefusedComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
  }
}
