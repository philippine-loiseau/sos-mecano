import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Quote } from '../../shared/models/mechanician/quote.model';
import { QuoteService } from '../../shared/services/mechanician/quote.service';
import { AlertService } from '../../shared/services/alert.service';
import { AD } from '../../shared/models/car-driver/ad.model';

@Component({
  selector: 'app-details-quote',
  templateUrl: './details-quote.component.html',
  styleUrls: ['./details-quote.component.scss']
})
export class DetailsQuoteComponent implements OnInit {

  title: string;
  closeBtnName: string;

  @Input() quote: Quote;
  @Input() ad: AD;

  constructor(public bsModalRef: BsModalRef, private quoteService: QuoteService, private alertService: AlertService) { }

  ngOnInit() {
    this.getInfoQuote();
  }

  getInfoQuote() {
    this.quoteService.getQuotesByAd(this.ad.id_ad).subscribe(nodeResponse => {
      if (nodeResponse.success) {
        for (let i = 0; i < nodeResponse.data.length; i++) {
          const tmp = nodeResponse.data[i] as Quote;
          if (tmp.accept_quote === this.quote.accept_quote) {
            this.quote = tmp;
          }
        }
      } else {
        this.alertService.showAlertErrorServer();
      }
    });
  }

}
