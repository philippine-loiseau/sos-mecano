import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Quote } from '../../shared/models/mechanician/quote.model';
import { AD } from '../../shared/models/car-driver/ad.model';
import { QuoteService } from '../../shared/services/mechanician/quote.service';
import { AlertService } from '../../shared/services/alert.service';
import * as moment from 'moment';

@Component({
  selector: 'app-quote-details',
  templateUrl: './quote-details.component.html',
  styleUrls: ['./quote-details.component.scss']
})
export class QuoteDetailsComponent implements OnInit {

  title: string;
  closeBtnName: string;
  ad: AD;

  @Input() quotes: Quote[] = [];

  constructor(public bsModalRef: BsModalRef, private alertService: AlertService, private quoteService: QuoteService) { }

  ngOnInit() {
    const id_ad = this.ad.id_ad;
    this.getQuotesByAd(id_ad);
  }

  getQuotesByAd(id_ad) {
    this.quoteService.getQuotesByAd(id_ad).subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.quotes = nodeResponse.data;
        for (let i = 0; i < this.quotes.length; i++) {
          this.quotes[i].date_quote = moment(this.quotes[i].date_quote).locale('fr').format('LL');
        }
      } else {
        this.alertService.showAlertErrorServer();
      }
    });
  }

  acceptQuote(quote: Quote) {
    this.quoteService.acceptQuote(quote).subscribe(nodeResponse => {
      this.alertService.showAlertQuoteAccepted();
      this.bsModalRef.hide();
    });
  }


  deleteQuote(quote: Quote) {
    this.quoteService.deleteQuote(this.quotes[0]).subscribe(nodeResponse => {
      this.alertService.showAlertQuoteRefused();
    });
  }

}
