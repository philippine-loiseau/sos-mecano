import { Component, OnInit, Input } from '@angular/core';
import { ClientSearchService } from '../../shared/services/car-driver/client-search.service';
import { AD } from '../../shared/models/car-driver/ad.model';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { QuoteAddComponent } from '../quote-add/quote-add.component';
import * as moment from 'moment';


@Component({
  selector: 'app-search-client',
  templateUrl: './search-client.component.html',
  styleUrls: ['./search-client.component.scss']
})
export class SearchClientComponent implements OnInit {


  @Input() ads: AD[] = [];

  loading = false;
  radius: number;
  searchTerm: string;
  bsModalRef: BsModalRef;

  constructor(private clientSearchService: ClientSearchService, private modalService: BsModalService) {
  }

  searchClient() {
    this.loading = true;
    this.clientSearchService.searchEntries(this.searchTerm, 30)
      .subscribe(response => {
        this.ads = response.data;
        for (let i = 0; i < this.ads.length; i++) {
          this.ads[i].repair_date = moment(this.ads[i].repair_date).locale('fr').format('LL');
        }
        this.loading = false;
      });
  }

  openQuoteAdd(ad: AD) {
    const initialState = {
      title: 'PROPOSER VOTRE DEVIS',
      ad: ad
    };
    this.bsModalRef = this.modalService.show(QuoteAddComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
  }


  ngOnInit() {
  }

}
