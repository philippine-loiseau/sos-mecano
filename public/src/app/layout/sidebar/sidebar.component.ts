import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
    isActive: boolean = false;
    showMenu: string = '';

    @Input() STEP1: string ;
    @Input() STEP2: string;
    @Input() STEP3: string;
    @Input() STEP4: string;

    @Input() i1: string;
    @Input() i2: string;
    @Input() i3: string;
    @Input() i4: string;


    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }
}
