import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-signup-navbar',
  templateUrl: './signup-navbar.component.html',
  styleUrls: ['./signup-navbar.component.scss']
})
export class SignupNavbarComponent implements OnInit {

  @Input() STEP1: string;
  @Input() STEP2: string;
  @Input() STEP3: string;
  @Input() STEP4: string;
  @Input() STEP5: string;
  
  @Input() iconStep1:string;
  @Input() iconStep2:string;
  @Input() iconStep3:string;
  @Input() iconStep4:string;
  @Input() iconStep5:string;

  constructor() { }

  ngOnInit() {
  }

}
