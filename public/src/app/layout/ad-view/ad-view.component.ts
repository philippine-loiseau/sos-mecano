import { Component, OnInit, Input, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AdService } from '../../shared/services/car-driver/ad.service';
import { AD } from '../../shared/models/car-driver/ad.model';
import { AdDetailsComponent } from '../ad-details/ad-details.component';
import { Constants } from '../../shared/models/constants';
import * as moment from 'moment';
import { AlertService } from '../../shared/services/alert.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ad-view',
  templateUrl: './ad-view.component.html',
  styleUrls: ['./ad-view.component.scss']
})
export class AdViewComponent implements OnInit {

  title: string;
  closeBtnName: string;
  updateBtnName: string;
  modalRef: BsModalRef;
  message: string;

  @Input() ads: AD[] = [];

  constructor(
    private router: Router,
    private modalService: BsModalService,
    public bsModalRef: BsModalRef,
    private adService: AdService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.getRepairAds();
  }

  openAdDetails(ad: AD) {
    const initialState = {
      title: 'DETAILS DE LA REPARATION',
      ad: ad
    };
    this.bsModalRef = this.modalService.show(AdDetailsComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'FERMER';
    this.bsModalRef.content.updateBtnName = 'MODIFIER';
  }

  openAdDelete(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  confirm(ad: AD): void {
    this.modalRef.hide();
    this.alertService.showAlertAdDelete();
    this.bsModalRef.hide();
    this.adService.deleteAd(ad).subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.ads = nodeResponse.data;
      } else if (nodeResponse.type === Constants.INFO_NO_ADS_FOUND) {
        this.alertService.showAlertErrorServer();
      }
    });
  }

  decline(): void {
    this.message = 'Declined!';
    this.modalRef.hide();
  }

  getRepairAds() {
    this.adService.getRepairAds().subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.ads = nodeResponse.data;
        if (this.ads.length === 0) {
          this.alertService.showAlertNotRepairAd();
        }
        for (let i = 0; i < this.ads.length; i++) {
          this.ads[i].date_ad = moment(this.ads[i].date_ad).locale('fr').format('LL');
          this.ads[i].repair_date = moment(this.ads[i].repair_date).locale('fr').format('LL');
        }
      } else {
        this.alertService.showAlertErrorServer();
      }
    });
  }
}
