import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCarDriverComponent } from './profile-car-driver.component';

describe('ProfileCarDriverComponent', () => {
  let component: ProfileCarDriverComponent;
  let fixture: ComponentFixture<ProfileCarDriverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileCarDriverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCarDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
