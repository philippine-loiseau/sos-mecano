import { Component, OnInit } from '@angular/core';
import { CarDriver } from '../../shared/models/car-driver/car-driver.model';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-car-driver',
  templateUrl: './profile-car-driver.component.html',
  styleUrls: ['./profile-car-driver.component.scss']
})
export class ProfileCarDriverComponent implements OnInit {
  carDriver: CarDriver = new CarDriver();

  constructor(public router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.currentUser();
  }

  currentUser() {
    this.authenticationService.getCurrentUser().subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.carDriver = nodeResponse.data;
      } else {
        this.router.navigate(['/login']);
      }
    });
  }
}
