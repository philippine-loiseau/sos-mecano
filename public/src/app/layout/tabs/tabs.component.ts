import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  @Input() TAB1: string;
  @Input() TAB2: string;
  @Input() TAB3: string;
  @Input() TAB4: string;
  @Input() TAB5: string;

  @Input() label1: string;
  @Input() label2: string;
  @Input() label3: string;
  @Input() label4: string;
  @Input() label5: string;


  constructor() { }

  ngOnInit() {
  }

}
