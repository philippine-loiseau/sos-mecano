import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { QuoteService } from '../../shared/services/mechanician/quote.service';
import * as moment from 'moment';
import { DetailsQuoteComponent } from '../details-quote/details-quote.component';
import { Quote } from '../../shared/models/mechanician/quote.model';
import { AlertService } from '../../shared/services/alert.service';
import { CarDetailsComponent } from '../car-details/car-details.component';
import { CarService } from '../../shared/services/car-driver/car.service';
import { AD } from '../../shared/models/car-driver/ad.model';
import { CarDriver } from '../../shared/models/car-driver/car-driver.model';

@Component({
  selector: 'app-quote-accept',
  templateUrl: './quote-accept.component.html',
  styleUrls: ['./quote-accept.component.scss']
})
export class QuoteAcceptComponent implements OnInit {


  title: string;
  closeBtnName: string;

  @Input() quotes = [];

  constructor(
    public bsModalRef: BsModalRef,
    private carService: CarService,
    private modalService: BsModalService,
    private alertService: AlertService,
    private quoteService: QuoteService) { }

  ngOnInit() {
    this.getQuoteAccepted();
  }

  openQuoteDetails(ad: AD) {
    this.quoteService.getValidatedQuoteByAd(ad.id_ad).subscribe(nodeResponse => {
      const quote = new  Quote();
      quote.accept_quote = true;
      const initialState = {
        title: 'DETAILS DU DEVIS',
        ad: ad,
        quote: quote
      };
      this.bsModalRef = this.modalService.show(DetailsQuoteComponent, { initialState });
      this.bsModalRef.content.closeBtnName = 'FERMER';
    });
  }

  openCarDetails(ad: AD) {
      const initialState = {
        title: 'DETAILS DE LA VOITURE',
        ad: ad
      };
      this.bsModalRef = this.modalService.show(CarDetailsComponent, { initialState });
      this.bsModalRef.content.closeBtnName = 'FERMER';
  }

  getQuoteAccepted() {
    this.quoteService.getQuoteAccepted().subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.quotes = nodeResponse.data;
        for (let i = 0; i < this.quotes.length; i++) {
          this.quotes[i].Ad.date_ad = moment(this.quotes[i].Ad.date_ad).locale('fr').format('LL');
        }
      } else {
        this.alertService.showAlertErrorServer();
      }
    });
  }
}
