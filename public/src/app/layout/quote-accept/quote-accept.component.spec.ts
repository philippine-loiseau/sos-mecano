import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteAcceptComponent } from './quote-accept.component';

describe('QuoteAcceptComponent', () => {
  let component: QuoteAcceptComponent;
  let fixture: ComponentFixture<QuoteAcceptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuoteAcceptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteAcceptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
