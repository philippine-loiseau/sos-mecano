import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-register-mechanician',
  templateUrl: './register-mechanician.component.html',
  styleUrls: ['./register-mechanician.component.scss']
})
export class RegisterMechanicianComponent implements OnInit {

  title: string;
  closeBtnName: string;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

}
