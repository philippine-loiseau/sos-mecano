import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterMechanicianComponent } from './register-mechanician.component';

describe('RegisterMechanicianComponent', () => {
  let component: RegisterMechanicianComponent;
  let fixture: ComponentFixture<RegisterMechanicianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterMechanicianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterMechanicianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
