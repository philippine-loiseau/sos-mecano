import { Component, OnInit, Input } from '@angular/core';
import { AD } from '../../shared/models/car-driver/ad.model';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-ad-update',
  templateUrl: './ad-update.component.html',
  styleUrls: ['./ad-update.component.scss']
})
export class AdUpdateComponent implements OnInit {

  title: string;
  closeBtnName: string;

  @Input() ad: AD;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

}
