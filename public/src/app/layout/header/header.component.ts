import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { AuthenticationService } from '../../shared/services/authentication.service';
import { CarDriver } from '../../shared/models/car-driver/car-driver.model';
import { Mechanician } from '../../shared/models/mechanician/mechanician.model';
import { AlertService } from '../../shared/services/alert.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  carDriver: CarDriver = new CarDriver;
  mechanician: Mechanician = new Mechanician;

  constructor(public router: Router, private authenticationService: AuthenticationService, private alertService: AlertService) {
  }

  ngOnInit() {
    const userType = JSON.parse(localStorage.getItem('token')).userType;
    this.currentUser(userType);
  }

  currentUser(userType) {
    this.authenticationService.getCurrentUser().subscribe(nodeResponse => {
      if (nodeResponse.success) {
        if (userType === 'driver') {
          this.carDriver = nodeResponse.data;
        } else {
          this.mechanician = nodeResponse.data;
        }
      } else {
        this.alertService.showAlertSessionExpired();
        this.router.navigate(['/login']);
      }
    });
  }

  logout(): void {
    localStorage.removeItem('token');
    this.router.navigate(['/home']);
  }
}
