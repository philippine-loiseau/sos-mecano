// IMPORT MODULE
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// IMPORT COMPONENT
import { SignupCarDriverComponent } from './signup-car-driver/signup-car-driver.component';
import { LoginDetailsComponent } from './signup-car-driver/login-details/login-details.component';
import { CarDetailsComponent } from './signup-car-driver/car-details/car-details.component';
import { ValidationComponent } from './signup-car-driver/validation/validation.component';
import { CarDriverDetailsComponent } from './signup-car-driver/car-driver-details/car-driver-details.component';
import { SignupLoginComponent } from '../../layout/signup-login/signup-login.component';

const routes: Routes = [
    {
        path: 'signup/car-driver/login', component: SignupCarDriverComponent,
        children: [
            { path: '', component: LoginDetailsComponent, outlet: 'signup-car-driver-view' },
        ]
    },
    {
        path: 'signup/car-driver/car-driver-details', component: SignupCarDriverComponent,
        children: [
            { path: '', component: CarDriverDetailsComponent, outlet: 'signup-car-driver-view' },
        ]
    },
    {
        path: 'signup/car-driver/car-details', component: SignupCarDriverComponent,
        children: [
            { path: '', component: CarDetailsComponent, outlet: 'signup-car-driver-view' },
        ]
    },
    {
        path: 'signup/car-driver/validation', component: SignupCarDriverComponent,
        children: [
            { path: '', component: ValidationComponent, outlet: 'signup-car-driver-view' },
        ]
    },
    {
        path: 'signup/connect', component: SignupCarDriverComponent,
        children: [
            { path: '', component: SignupLoginComponent, outlet: 'signup-car-driver-view' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]

})
export class SignupCarDriverRoutingModule {
}
