import { CarDriver } from '../../../shared/models/car-driver/car-driver.model';
import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../shared/router.animations';
import { SignupCarDriverService } from '../../../shared/services/car-driver/signup-car-driver.service';


@Component({
  selector: 'app-signup-car-driver',
  templateUrl: './signup-car-driver.component.html',
  styleUrls: ['./signup-car-driver.component.scss'],
  animations: [routerTransition()]
})
export class SignupCarDriverComponent implements OnInit {

  routerLogin = '/signup/car-driver/login';
  routerCarDriverDetails = '/signup/car-driver/car-driver-details';
  routerCarDetails = '/signup/car-driver/car-details';
  routerValidation = '/signup/car-driver/validation';
  routerConnection = '/signup/connect';

  iconLogin = 'fa fa-address-card';
  iconUserAdd = 'fa fa-user-plus';
  iconCar = 'fa fa-car';
  iconValidation = 'fa fa-check';
  iconConnection = 'fa fa-user';

  @Input() CarDriver;

  constructor(private signupCarDriverService: SignupCarDriverService) { }

  ngOnInit() {
  }

}
