import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SignupCarDriverService } from '../../../../shared/services/car-driver/signup-car-driver.service';
import { CarDriver } from '../../../../shared/models/car-driver/car-driver.model';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.scss']
})
export class CarDetailsComponent implements OnInit {

  title = 'Merci de remplir les informations de votre voiture.';
  carDriver: CarDriver;
  form: any;

  constructor(
    private router: Router,
    private signupCarDriverService: SignupCarDriverService) {
  }

  ngOnInit() {
    this.carDriver = this.signupCarDriverService.getCarDriver();
  }

  onPhotoCarChanged(event) {
    this.fileChanged(event, 'photo_car');
  }

  fileChanged(event, fileName) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {

        if ('photo_car' === fileName) {
          this.carDriver.photo_user = reader.result.split(',')[1];
        }
      };
    }
  }

  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }

    this.signupCarDriverService.setCarDetails(this.carDriver);
    return true;
  }

  goToPrevious(form: any) {
    this.signupCarDriverService.setCarDetails(this.carDriver);
    this.router.navigate(['signup/car-driver/car-driver-details']);
  }

  goToNext(form: any) {
    if (this.save(form)) {
      this.router.navigate(['signup/car-driver/validation']);
    }
  }
}
