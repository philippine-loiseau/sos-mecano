import { Component, OnInit } from '@angular/core';
import { CarDriver } from '../../../../shared/models/car-driver/car-driver.model';
import { SignupCarDriverService } from '../../../../shared/services/car-driver/signup-car-driver.service';
import { Router } from '@angular/router';
import { NodeResponse } from '../../../../shared/models/node_response';

@Component({
  selector: 'app-car-driver-details',
  templateUrl: './car-driver-details.component.html',
  styleUrls: ['./car-driver-details.component.scss']
})
export class CarDriverDetailsComponent implements OnInit {
  title = 'Merci de remplir les informations ci-dessous.';
  carDriver: CarDriver;
  form: any;

  constructor(
    private router: Router,
    private signupCarDriverService: SignupCarDriverService) {
  }

  ngOnInit() {
    this.carDriver = this.signupCarDriverService.getCarDriver();
  }

  onPhotoUserChanged(event) {
    this.fileChanged(event, 'photo_user');
  }

  fileChanged(event, fileName) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if ('photo_user' === fileName) {
          this.carDriver.photo_user = reader.result.split(',')[1];
        }
      };
    }
  }

  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }

    this.signupCarDriverService.setCarDriverDetails(this.carDriver);
    return true;
  }

  goToPrevious() {
    this.signupCarDriverService.setCarDriverDetails(this.carDriver);
    this.router.navigate(['signup/car-driver/login']);

  }

  goToNext(form: any) {
    if (this.save(form)) {
      this.router.navigate(['signup/car-driver/car-details']);
    }
  }
}
