import { Component, OnInit } from '@angular/core';
import { CarDriver } from '../../../../shared/models/car-driver/car-driver.model';
import { SignupCarDriverService } from '../../../../shared/services/car-driver/signup-car-driver.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-details',
  templateUrl: './login-details.component.html',
  styleUrls: ['./login-details.component.scss']
})
export class LoginDetailsComponent implements OnInit {
  title = 'Merci de remplir vos identifiants pour vos prochaines connexions.';
  carDriver: CarDriver;
  form: any;

  constructor(
    private router: Router,
    private signupCarDriverService: SignupCarDriverService) {
  }

  ngOnInit() {
    this.carDriver = this.signupCarDriverService.getCarDriver();
  }

  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }

    this.signupCarDriverService.setLoginDetails(this.carDriver);
    return true;
  }

  goToNext(form: any) {
    if (this.save(form)) {
      // Navigate to the work page
      this.router.navigate(['signup/car-driver/car-driver-details']);
    }
  }
}
