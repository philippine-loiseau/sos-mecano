import { Component, OnInit, Input } from '@angular/core';
import { CarDriver } from '../../../../shared/models/car-driver/car-driver.model';
import { SignupCarDriverService } from '../../../../shared/services/car-driver/signup-car-driver.service';
import { Router } from '@angular/router';
import { AlertService } from '../../../../shared/services/alert.service';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})
export class ValidationComponent implements OnInit {

  title = ' Voici le récapitulatif des informations que vous avez rentré.';
  @Input() carDriver: CarDriver;
  isFormValid = false;

  constructor(private router: Router, private signupCarDriverService: SignupCarDriverService, private alertService: AlertService) {
  }

  ngOnInit() {
    this.carDriver = this.signupCarDriverService.getCarDriver();
    this.isFormValid = this.signupCarDriverService.isFormValid();
  }

  submit() {
    this.isFormValid = false;
    this.signupCarDriverService.register(this.carDriver)
      .subscribe(nodeResponse => {
        if (nodeResponse.success) {
          localStorage.setItem('submit', 'true');
          this.alertService.showAlertSignup();
          this.router.navigate(['signup/connect']);
        } else {
          this.alertService.showAlertErrorServer();
        }
      });
  }
}
