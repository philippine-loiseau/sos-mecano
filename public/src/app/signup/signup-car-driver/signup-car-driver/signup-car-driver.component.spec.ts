import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupCarDriverComponent } from './signup-car-driver.component';

describe('SignupCarDriverComponent', () => {
  let component: SignupCarDriverComponent;
  let fixture: ComponentFixture<SignupCarDriverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupCarDriverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupCarDriverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
