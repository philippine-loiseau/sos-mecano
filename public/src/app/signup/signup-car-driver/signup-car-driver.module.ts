// IMPORT MODULE
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '../../layout/layout.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

// IMPORT ROUTING MODULE
import { SignupCarDriverRoutingModule } from './signup-car-driver-routing.module';

// IMPORT COMPONENT
import { SignupCarDriverComponent } from './signup-car-driver/signup-car-driver.component';
import { LoginDetailsComponent } from './signup-car-driver/login-details/login-details.component';
import { CarDriverDetailsComponent } from './signup-car-driver/car-driver-details/car-driver-details.component';
import { CarDetailsComponent } from './signup-car-driver/car-details/car-details.component';
import { ValidationComponent } from './signup-car-driver/validation/validation.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SignupCarDriverRoutingModule,
    LayoutModule,
    BsDatepickerModule
  ],
  declarations: [SignupCarDriverComponent, LoginDetailsComponent, CarDriverDetailsComponent, CarDetailsComponent, ValidationComponent]
})

export class SignupCarDriverModule { }
