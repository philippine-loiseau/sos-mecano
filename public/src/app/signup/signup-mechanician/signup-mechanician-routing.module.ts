import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignupMechanicianComponent } from './signup-mechanician/signup-mechanician.component';
import { LoginDetailsComponent } from './signup-mechanician/login-details/login-details.component';
import { CompanyDetailsComponent } from './signup-mechanician/company-details/company-details.component';
import { WorkSchedulesComponent } from './signup-mechanician/work-schedules/work-schedules.component';
import { ValidationComponent } from './signup-mechanician/validation/validation.component';
import { SignupLoginComponent } from '../../layout/signup-login/signup-login.component';


const routes: Routes = [
    {
        path: 'signup/mechanician/login', component: SignupMechanicianComponent,
        children: [
            { path: '', component: LoginDetailsComponent, outlet: 'signup-mechanician-view' },
        ]
    },
    {
        path: 'signup/mechanician/company-details', component: SignupMechanicianComponent,
        children: [
            { path: '', component: CompanyDetailsComponent, outlet: 'signup-mechanician-view' },
        ]
    },
    {
        path: 'signup/mechanician/work-schedules', component: SignupMechanicianComponent,
        children: [
            { path: '', component: WorkSchedulesComponent, outlet: 'signup-mechanician-view' },
        ]
    },
    {
        path: 'signup/mechanician/validation', component: SignupMechanicianComponent,
        children: [
            { path: '', component: ValidationComponent, outlet: 'signup-mechanician-view' },
        ]
    },
    {
        path: 'signup/connect', component: SignupMechanicianComponent,
        children: [
            { path: '', component: SignupLoginComponent, outlet: 'signup-mechanician-view' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SignupMechanicianRoutingModule {
}
