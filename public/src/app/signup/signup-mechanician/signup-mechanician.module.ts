// IMPORT MODULE
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from '../../layout/layout.module';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';

// IMPORT ROUTING
import { SignupMechanicianRoutingModule } from './signup-mechanician-routing.module';

// IMPORT COMPONENT
import { SignupMechanicianComponent } from './signup-mechanician/signup-mechanician.component';
import { LoginDetailsComponent } from './signup-mechanician/login-details/login-details.component';
import { CompanyDetailsComponent } from './signup-mechanician/company-details/company-details.component';
import { WorkSchedulesComponent } from './signup-mechanician/work-schedules/work-schedules.component';
import { ValidationComponent } from './signup-mechanician/validation/validation.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    LayoutModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    SignupMechanicianRoutingModule
  ],
  declarations: [SignupMechanicianComponent, LoginDetailsComponent, CompanyDetailsComponent, WorkSchedulesComponent, ValidationComponent],
  exports: [],
  providers: []
})
export class SignupMechanicianModule { }
