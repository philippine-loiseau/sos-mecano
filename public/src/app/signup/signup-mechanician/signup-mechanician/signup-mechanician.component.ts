import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../shared/router.animations';
import { SignupMechanicianService } from '../../../shared/services/mechanician/signup-mechanician.service';

@Component({
  selector: 'app-signup-mechanician',
  templateUrl: './signup-mechanician.component.html',
  styleUrls: ['./signup-mechanician.component.scss'],
  animations: [routerTransition()]
})
export class SignupMechanicianComponent implements OnInit {

  routerLogin = '/signup/mechanician/login';
  routerCompany = '/signup/mechanician/company-details';
  routerWorkSchedules = '/signup/mechanician/work-schedules';
  routerValidation = '/signup/mechanician/validation';
  routerConnection = '/signup/connect';

  iconLogin = 'fa fa-address-card';
  iconCompany = 'fa fa-building';
  iconWorkSchedules = 'fa fa-calendar';
  iconValidation = 'fa fa-check';
  iconConnection = 'fa fa-user';


  @Input() MechanicianDetails;

  constructor(private signupMechanicianService: SignupMechanicianService) { }

  ngOnInit() {
    this.MechanicianDetails = this.signupMechanicianService.getMechanician();
  }
}
