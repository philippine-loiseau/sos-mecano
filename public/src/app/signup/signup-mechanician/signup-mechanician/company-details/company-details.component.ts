import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { SignupMechanicianService } from '../../../../shared/services/mechanician/signup-mechanician.service';
import { NodeResponse } from '../../../../shared/models/node_response';
import { Observable } from 'rxjs/Observable';
import { Mechanician } from '../../../../shared/models/mechanician/mechanician.model';
import { AlertService } from '../../../../shared/services/alert.service';



@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.scss']
})
export class CompanyDetailsComponent implements OnInit {
  title = 'Informations de votre entreprise';
  mechanician: Mechanician;

  constructor(
    private router: Router,
    private signupMechanicianService: SignupMechanicianService,
    private alertService: AlertService
    ) {
  }


  ngOnInit() {
    this.mechanician = this.signupMechanicianService.getMechanician();
  }

  onKbisChanged(event) {
    this.fileChanged(event, 'kbis');
  }

  fileChanged(event, fileName) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      const file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if ('kbis' === fileName) {
          this.mechanician.kbis = reader.result.split(',')[1];
          this.signupMechanicianService.savePDFAndCreateCompanyId(this.mechanician)
          .subscribe(nodeResponse => {
            if (nodeResponse.success) {
              this.mechanician.id_co = nodeResponse.data.id_co;
              this.mechanician.kbis = '';
            } else {
              this.alertService.showAlertErrorServer();
            }
          });
        }
      };
    }
  }

  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }
    this.signupMechanicianService.setCompanyDetails(this.mechanician);
    return true;
  }


  goToPrevious(form: any) {
    this.signupMechanicianService.setCompanyDetails(this.mechanician);
    this.router.navigate(['signup/mechanician/login']);
  }

  goToNext(form: any) {
    if (this.save(form)) {
      this.router.navigate(['signup/mechanician/work-schedules']);
    }
  }
}
