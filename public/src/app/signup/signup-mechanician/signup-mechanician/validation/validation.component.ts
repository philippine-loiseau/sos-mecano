import { Component, OnInit, Input } from '@angular/core';
import { SignupMechanicianService } from '../../../../shared/services/mechanician/signup-mechanician.service';
import { Mechanician } from '../../../../shared/models/mechanician/mechanician.model';
import { Router } from '@angular/router';
import { AlertService } from '../../../../shared/services/alert.service';

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.scss']
})

export class ValidationComponent implements OnInit {
  title = ' Voici le récapitulatif des informations que vous avez rentré.';

  @Input() mechanician: Mechanician;
  isFormValid = false;

  constructor(private router: Router, private signupMechanicianService: SignupMechanicianService, private alertService: AlertService) {
  }

  ngOnInit() {
    this.mechanician = this.signupMechanicianService.getMechanician();
    this.isFormValid = this.signupMechanicianService.isFormValid();
  }

  submit() {
    this.isFormValid = false;
    this.signupMechanicianService.register(this.mechanician)
    .subscribe(nodeResponse => {
      if (nodeResponse.success) {
        this.alertService.showAlertSignup();
        localStorage.setItem('submit', 'true');
        this.router.navigate(['signup/connect']);
      } else {
        this.alertService.showAlertErrorServer();
      }
    });
  }
}
