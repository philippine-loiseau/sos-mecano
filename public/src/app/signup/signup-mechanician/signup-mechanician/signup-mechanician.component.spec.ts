import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupMechanicianComponent } from './signup-mechanician.component';

describe('SignupMechanicianComponent', () => {
  let component: SignupMechanicianComponent;
  let fixture: ComponentFixture<SignupMechanicianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupMechanicianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupMechanicianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
