import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SignupMechanicianService } from '../../../../shared/services/mechanician/signup-mechanician.service';
import { Mechanician } from '../../../../shared/models/mechanician/mechanician.model';

@Component({
    selector: 'app-login-details',
    templateUrl: './login-details.component.html'
})

export class LoginDetailsComponent implements OnInit {
    title = 'Saisissez vos identifiants pour vos prochaines connexions';
    mechanician: Mechanician;
    form: any;

    constructor(
        private router: Router,
        private signupMechanicianService: SignupMechanicianService) {
    }

    ngOnInit() {
        this.mechanician = this.signupMechanicianService.getMechanician();
    }

    save(form: any): boolean {
        if (!form.valid) {
            return false;
        }

        this.signupMechanicianService.setLoginDetails(this.mechanician);
        return true;
    }

    goToNext(form: any) {
        if (this.save(form)) {
            this.router.navigate(['signup/mechanician/company-details']);
        }
    }
}
