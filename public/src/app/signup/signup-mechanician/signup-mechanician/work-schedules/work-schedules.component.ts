import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Mechanician, WorkSchedules } from '../../../../shared/models/mechanician/mechanician.model';
import { SignupMechanicianService } from '../../../../shared/services/mechanician/signup-mechanician.service';

@Component({
  selector: 'app-work-schedules',
  templateUrl: './work-schedules.component.html',
  styleUrls: ['./work-schedules.component.scss']
})
export class WorkSchedulesComponent implements OnInit {
  title = 'Horaires de travail';
  workSchedules: WorkSchedules;
  form: any;

  isMeridian = false;
  showSpinners = true;
  myTime: Date = new Date();

  question: string;
  visible: boolean;
  answer: Array<string>;

  constructor(
    private router: Router,
    private signupMechanicianService: SignupMechanicianService) {
  }

  ngOnInit() {
    this.workSchedules = this.signupMechanicianService.getWorkSchedules();
  }

  toggleMode(): void {
    this.isMeridian = !this.isMeridian;
  }


  save(form: any): boolean {
    if (!form.valid) {
      return false;
    }
    this.signupMechanicianService.setWorkSchedules(this.workSchedules);
    return true;
  }


  goToPrevious(form: any) {
    this.signupMechanicianService.setWorkSchedules(this.workSchedules);
    this.router.navigate(['signup/mechanician/company-details']);
  }

  goToNext(form: any) {
    if (this.save(form)) {
      this.router.navigate(['signup/mechanician/validation']);
    }
  }

}
