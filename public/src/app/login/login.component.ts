import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../shared/router.animations';
import { AuthenticationService } from '../shared/services/authentication.service';
import { Constants } from '../shared/models/constants';
import { AlertService } from '../shared/services/alert.service';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    model: any = {};

    constructor(public router: Router, private authenticationService: AuthenticationService, private alertService: AlertService) {
        this.authenticationService.logout();

    }

    ngOnInit() {
    }

    onLoggedin() {
        this.authenticationService.login(this.model.email, this.model.password).subscribe(response => {
            if (response.success) {
                if (response.type === Constants.INFO_USER_CONNECTED) {
                    this.router.navigate(['car-driver/home']);
                } else if (response.type === Constants.INFO_COMPANY_CONNECTED) {
                    this.router.navigate(['mechanician/home']);
                } else {
                    this.alertService.showAlertErroID();
                }
            }
        }, error => {
            this.alertService.showAlertErrorServer();
        });

    }
}

