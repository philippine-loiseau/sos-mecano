export class Quote {
    price_estimate: number;
    date_quote: string;
    description_quote: string;
    name_quote: string;
    accept_quote: boolean;
    id_co: number;
    id_ad: string;
    userId: string;
}
