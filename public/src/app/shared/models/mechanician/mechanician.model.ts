export class Mechanician {

    // ID
    id_co: number;
    // Company name
    name_co: string;
    // SIRET
    siret: string;
    // NAF
    naf: string;
    // KBIS scanned
    kbis: string;
    // Phone
    tel_co: string;
    // Address
    address_co: string;
    // Postal Code
    postal_code_co: string;
    // City
    city_co: string;
    // Boss, owner
    boss_co: string;
    // Company date of creation
    creation_co: Date;
    // Password
    password_co: string;
    // Email company
    email_co: string;
    // Website company
    website_co: string;

    workSchedules: WorkSchedules = new WorkSchedules;

    clear() {
        this.id_co = 0;
        // Company name
        this.name_co = '';
        // SIRET
        this.siret = '';
        // NAF
        this.naf = '';
        // KBIS scanned
        this.kbis = '';
        // Phone
        this.tel_co = '';
        // Address
        this.address_co = '';
        // Postal Code
        this.postal_code_co = '';
        // City
        this.city_co = '';
        // Boss, owner
        this.boss_co = '';
        // Company date of creation
        // tslint:disable-next-line:no-unused-expression
        this.creation_co;
        // Password
        this.password_co = '';
        // Email company
        this.email_co = '';
        // Website company
        this.website_co = '';

        this.workSchedules = new WorkSchedules;
    }
}

export class WorkSchedules {
    workDay: WorkDay[] = schedules;
}

export class WorkDay {
    day: string;
    opening_time: Date;
    closing_time: Date;
    night_service: boolean;
}

const schedules: WorkDay[] = [
    {
        day: 'Lundi',
        opening_time: null,
        closing_time: null,
        night_service: false,
    },
    {
        day: 'Mardi',
        opening_time: null,
        closing_time: null,
        night_service: false,
    },
    {
        day: 'Mercredi',
        opening_time: null,
        closing_time: null,
        night_service: false,
    },
    {
        day: 'Jeudi',
        opening_time: null,
        closing_time: null,
        night_service: false,
    },
    {
        day: 'Vendredi',
        opening_time: null,
        closing_time: null,
        night_service: false,
    },
    {
        day: 'Samedi',
        opening_time: null,
        closing_time: null,
        night_service: false,
    },
    {
        day: 'Dimanche',
        opening_time: null,
        closing_time: null,
        night_service: false,
    }
];
