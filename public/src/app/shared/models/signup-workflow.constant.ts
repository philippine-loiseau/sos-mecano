export const STEPS_MECHANICIAN = {
    loginDetails: 'loginDetails',
    companyDetails: 'companyDetails',
    workSchedules: 'workSchedules',
    validation: 'validation'
};

export const STEPS_CARDRIVER = {
    loginDetails: 'loginDetails',
    carDriverDetails: 'carDriverDetails',
    carDetails: 'carDetails',
    validation: 'validation'
};
