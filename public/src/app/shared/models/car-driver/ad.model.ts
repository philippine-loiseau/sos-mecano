export class AD {
    id_ad: string;
    name_ad: string;
    date_ad: string;
    description_ad: string;
    photo_ad: string;
    longitude: number;
    latitude: number;
    distance: number;
    repair_date: string;
    ad_type: string;
    accept_ad: boolean;
    token: number;
    userId: string;
    id_car: string;
}


