export class CarDriver {

    id_user = '';
    email_user = '';
    password_user = '';
    name_user = '';
    first_name_user = '';
    tel_user = '';
    address_user = '';
    postal_code_user = '';
    city_user = '';
    photo_user = '';
    car_brand = '';
    car_version = '';
    car_model = '';
    in_service = '';
    photo_car = '';
    license_plate = '';
    serial_number = '';
    engine = '';
    id_car = '';

    clear() {
        this.id_user = '';
        this.email_user = '';
        this.password_user = '';
        this.name_user = '';
        this.first_name_user = '';
        this.tel_user = '';
        this.address_user = '';
        this.postal_code_user = '';
        this.city_user = '';
        this.photo_user = '';
        this.email_user = '';
        this.car_brand = '';
        this.car_version = '';
        this.car_model = '';
        this.in_service = '';
        this.photo_car = '';
        this.license_plate = '';
        this.serial_number = '';
        this.engine = '';
    }
}
