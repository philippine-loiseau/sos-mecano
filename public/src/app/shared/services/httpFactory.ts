import { XHRBackend, Http, RequestOptions } from '@angular/http';
import { TokenInterceptor } from './token.interceptor';
import { Router } from '@angular/router';

export function httpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions, router: Router): Http {
    return new TokenInterceptor(xhrBackend, requestOptions, router);
}
