import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { NodeResponse } from '../../models/node_response';

@Injectable()
export class ClientSearchService {

  baseUrl = 'http://localhost:3000/api';
  searchUrl = this.baseUrl + '/ads/search?ad_type=repair&term=';
  position: Position;

  constructor(private http: Http) {
    if (navigator.geolocation) {

      navigator.geolocation.getCurrentPosition((position) => {
        this.position = position;
      });
    }
  }

  search(terms: Observable<string>, radius: number) {
    return terms
      .debounceTime(400)
      .distinctUntilChanged()
      .switchMap(term => this.searchEntries(term, radius));
  }

  searchEntries(term, radius): Observable<NodeResponse> {
    return this.http
      .get(this.searchUrl + term + '&radius=' + radius + '&latitude=' + this.position.coords.latitude
        + '&longitude=' + this.position.coords.longitude)
      .map((response: Response) => {
        const nodeResponse: NodeResponse = JSON.parse(response.json());
        return nodeResponse;
      });
  }
}
