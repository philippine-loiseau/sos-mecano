import { Injectable } from '@angular/core';

import { CarDriver } from './../../models/car-driver/car-driver.model';
import { SignupCarDriverWorkflowService } from './signup-car-driver-workflow.service';
import { STEPS_CARDRIVER } from './../../models/signup-workflow.constant';
import { NodeResponse } from '../../models/node_response';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Mechanician } from '../../models/mechanician/mechanician.model';


@Injectable()
export class SignupCarDriverService {

    private carDriver: CarDriver = new CarDriver();
    private isLoginFormValid = false;
    private isCarDetailFormValid = false;
    private isCarFormValid = false;

    baseUrl = 'http://localhost:3000/api';
    createUserUrl = this.baseUrl + '/users/createUser';


    constructor(private http: Http, private signupCarDriverWorkflowService: SignupCarDriverWorkflowService) {
    }


    setLoginDetails(carDriver: CarDriver) {

        this.isLoginFormValid = true;
        this.carDriver.email_user = carDriver.email_user;
        this.carDriver.password_user = carDriver.password_user;

        this.signupCarDriverWorkflowService.validateStep(STEPS_CARDRIVER.loginDetails);
    }

    setCarDriverDetails(carDriver: CarDriver) {

        this.isCarDetailFormValid = true;
        this.carDriver.name_user = carDriver.name_user;
        this.carDriver.first_name_user = carDriver.first_name_user;
        this.carDriver.photo_user = carDriver.photo_user;
        this.carDriver.address_user = carDriver.address_user;
        this.carDriver.postal_code_user = carDriver.postal_code_user;
        this.carDriver.city_user = carDriver.city_user;
        this.carDriver.tel_user = carDriver.tel_user;

        this.signupCarDriverWorkflowService.validateStep(STEPS_CARDRIVER.carDriverDetails);
    }

    setCarDetails(carDriver: CarDriver) {

        this.isCarFormValid = true;
        this.carDriver.in_service = carDriver.in_service;
        this.carDriver.car_brand = carDriver.car_brand;
        this.carDriver.car_model = carDriver.car_model;
        this.carDriver.car_version = carDriver.car_version;
        this.carDriver.license_plate = carDriver.license_plate;
        this.carDriver.serial_number = carDriver.serial_number;
        this.carDriver.photo_car = carDriver.photo_car;
        this.carDriver.engine = carDriver.engine;

        this.signupCarDriverWorkflowService.validateStep(STEPS_CARDRIVER.carDetails);
    }


    savePhotoUserAndCreateUserId(data: CarDriver): Observable<NodeResponse> {
        return this.http.post(this.createUserUrl, data).map((response => {
            const nodeResponse: NodeResponse = JSON.parse(response.json());
            return nodeResponse;
        }));
    }


    getCarDriver(): CarDriver {
        return this.carDriver;
    }

    resetCarDriver(): CarDriver {
        // Reset the workflow
        this.signupCarDriverWorkflowService.resetSteps();
        // Return the form data after all this.* members had been reset
        this.carDriver.clear();
        this.isLoginFormValid = this.isCarDetailFormValid = this.isCarFormValid = false;
        return this.carDriver;
    }

    isFormValid() {
        // Return true if all forms had been validated successfully; otherwise, return false
        return this.isLoginFormValid &&
            this.isCarDetailFormValid &&
            this.isCarFormValid;
    }

    register(data: CarDriver): Observable<NodeResponse> {
        return this.http.post(this.createUserUrl, data)
            .map((response => {
                const nodeResponse: NodeResponse = JSON.parse(response.json());
                return nodeResponse;
            }));
    }
}
