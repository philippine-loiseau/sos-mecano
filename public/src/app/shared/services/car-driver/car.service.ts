import { Injectable } from '@angular/core';
import { NodeResponse } from '../../models/node_response';
import { Observable } from 'rxjs/Observable';

import { Http, RequestOptions, Headers } from '@angular/http';
import { CarDriver } from '../../models/car-driver/car-driver.model';

@Injectable()
export class CarService {

    private car: CarDriver = new CarDriver();

    baseUrl = 'http://localhost:3000/api';
    getCarUrl = this.baseUrl + '/car/getUserCars';
    getCarByAdByUrl = this.baseUrl + '/car/getCar';

    constructor(private http: Http) { }

    setCar(car: CarDriver) {
        this.car.engine = car.engine;
        this.car.car_brand = car.car_brand;
        this.car.car_model = car.car_model;
        this.car.car_version = car.car_version;
        this.car.photo_car = car.photo_car;
        this.car.serial_number = car.serial_number;
        this.car.license_plate = car.license_plate;
        this.car.in_service = car.in_service;
    }

    getCar(): CarDriver {
        return this.car;
    }

    getUserCar(): Observable<NodeResponse> {
        return this.http.get(this.getCarUrl).map(response => {
            const nodeResponse: NodeResponse = JSON.parse(response.json());
            return nodeResponse;
        });
    }

    getCarByAd(idCar): Observable<NodeResponse> {
        return this.http.get(this.getCarByAdByUrl + '?carId=' + idCar).map(response => {
            const nodeResponse: NodeResponse = JSON.parse(response.json());
            return nodeResponse;
        });
    }

}
