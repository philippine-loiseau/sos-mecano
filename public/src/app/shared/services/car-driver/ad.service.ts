import { Injectable } from '@angular/core';
import { AD } from '../../models/car-driver/ad.model';
import { NodeResponse } from '../../models/node_response';
import { Observable } from 'rxjs/Observable';

import { Http, RequestOptions, Headers } from '@angular/http';

@Injectable()
export class AdService {

  private ad: AD = new AD();

  baseUrl = 'http://localhost:3000/api';
  createAdUrl = this.baseUrl + '/ads/createAd';
  getAdsUrl = this.baseUrl + '/ads/getDefaultAdsByUser';
  deleteAdUrl = this.baseUrl + '/ads/deleteAd';
  getAdsWithQuotesUrl = this.baseUrl + '/ads/getAdsWithQuotes';
  findAdByIdUrl = this.baseUrl + '/ads/getAdById?idAd=';

  constructor(private http: Http) { }

  setAd(ad: AD) {

    this.ad.name_ad = ad.name_ad;
    this.ad.photo_ad = ad.photo_ad;
    this.ad.repair_date = ad.repair_date;
    this.ad.ad_type = ad.ad_type;
    this.ad.accept_ad = ad.accept_ad;
    this.ad.longitude = ad.longitude;
    this.ad.latitude = ad.latitude;
    this.ad.token = ad.token;
    this.ad.distance = ad.distance;
    this.ad.description_ad = ad.description_ad;
    this.ad.date_ad = ad.date_ad;
    this.ad.id_car = ad.id_car;
  }

  getAd(): AD {
    return this.ad;
  }

  getAdById(data: string): Observable<NodeResponse> {
    return this.http.get(this.findAdByIdUrl +  data).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

  createAd(data: AD): Observable<NodeResponse> {
    data.userId = JSON.parse(localStorage.getItem('token')).user;
    return this.http.post(this.createAdUrl, data).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

  getRepairAds(): Observable<NodeResponse> {
    const userId = JSON.parse(localStorage.getItem('token')).user;
    return this.http.get(this.getAdsUrl).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

  deleteAd(data: AD): Observable<NodeResponse> {
    data.userId = JSON.parse(localStorage.getItem('token')).user;
    return this.http.post(this.deleteAdUrl, data).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

  getAdsWithQuotesByUser(): Observable<NodeResponse> {
    return this.http.get(this.getAdsWithQuotesUrl).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

}
