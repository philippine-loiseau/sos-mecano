import { Injectable } from '@angular/core';
import swal from 'sweetalert2';

@Injectable()
export class AlertService {

  constructor() {
  }

  // ALERT ERROR
  showAlertErroID() {
    swal({
      type: 'error',
      title: 'Oops...',
      text: 'Votre identifiant ou mot passe est incorrect',
      confirmButtonColor: 'black',
    });
  }

  showAlertSessionExpired() {
    swal({
      type: 'error',
      title: 'Oops...',
      text: 'Veuillez vous reconnecter.',
      confirmButtonColor: 'black',
    });
  }

  showAlertErrorServer() {
    swal({
      type: 'error',
      title: 'Oops...',
      text: 'Un problème technique est survenu, nous essayons de rétablir l\'accès au site le plus vite possible.',
      confirmButtonColor: 'black',
    });
  }

  // ALERT QUOTE
  showAlertQuoteAccepted() {
    swal({
      title: 'Devis accepté',
      text: 'Un mécanicien va vous contacter pour convenir d\'un rendez-vous',
      type: 'success',
      confirmButtonColor: '#3085d6',
    });
  }

  showAlertQuoteRefused() {
    swal({
      title: 'Devis refusé',
      type: 'error',
      confirmButtonColor: '#3085d6',
    });
  }

  showAlertQuoteAdd() {
    swal({
      title: 'Votre devis a bien été pris en compte.',
      text: 'Vous receverez une notification si il a été accepté',
      type: 'success',
      confirmButtonColor: 'black',
    });
  }

  showAlertQuoteDelete() {
    swal({
      title: 'Votre devis a été supprimé.',
      type: 'success',
      confirmButtonColor: 'black',
    });
  }

  // ALERT AD
  showAlertAdAdd() {
    swal({
      title: 'Votre demande de devis a bien été pris en compte.',
      text: 'Vous receverez une notification si vous avez des offres aux meilleurs prix.',
      type: 'success',
      confirmButtonColor: 'black',
    });
  }

  showAlertErroAdAdd() {
    swal({
      type: 'error',
      title: 'Oops...',
      text: 'Veuillez remplir tout les champs de saisies.',
      confirmButtonColor: 'black',
    });
  }

  showAlertAdDelete() {
    swal({
      title: 'Votre demande de devis a été supprimé.',
      type: 'success',
      confirmButtonColor: 'black',
    });
  }

  showAlertNotRepairAd() {
    swal({
      type: 'error',
      title: 'Oops...',
      text: 'Vous n\'avez aucune demande de réparation',
      confirmButtonColor: 'black',
    });
  }

  // ALERT SIGNUP
  showAlertSignup() {
    swal({
      title: 'Votre inscription a bien été prise en compte, connectez-vous !',
      type: 'success',
      confirmButtonColor: 'black',
    });
  }


  // ALERT CAR
  showAlertNotCar() {
    swal({
      type: 'error',
      title: 'Oops...',
      text: 'Vous n\'avez aucune voiture enregistrée',
      confirmButtonColor: 'black',
    });
  }
}

