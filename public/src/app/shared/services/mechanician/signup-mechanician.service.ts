import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { Mechanician, WorkSchedules } from '../../models/mechanician/mechanician.model';
import { SignupMechanicianWorkflowService } from './signup-mechanician-workflow.service';
import { STEPS_MECHANICIAN } from '../../models/signup-workflow.constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NodeResponse } from '../../models/node_response';



@Injectable()
export class SignupMechanicianService {

    private mechanician: Mechanician = new Mechanician();
    private isLoginFormValid = false;
    private isCompanyFormValid = false;
    private isScheduleFormValid = false;


    baseUrl = 'http://localhost:3000/api';
    createCompanyUrl: string = this.baseUrl + '/company/createCompany';

    constructor(private http: Http, private signupMechanicianWorkflowService: SignupMechanicianWorkflowService) {
     }

    setLoginDetails(mechanician: Mechanician) {

        this.isLoginFormValid = true;
        this.mechanician.email_co = mechanician.email_co;
        this.mechanician.password_co = mechanician.password_co;

        this.signupMechanicianWorkflowService.validateStep(STEPS_MECHANICIAN.loginDetails);
    }

    setCompanyDetails(mechanician: Mechanician) {

        this.isCompanyFormValid = true;
        this.mechanician.id_co = mechanician.id_co,
            this.mechanician.creation_co = mechanician.creation_co,
            this.mechanician.kbis = mechanician.kbis,
            this.mechanician.naf = mechanician.naf,
            this.mechanician.name_co = mechanician.name_co,
            this.mechanician.postal_code_co = mechanician.postal_code_co,
            this.mechanician.siret = mechanician.siret,
            this.mechanician.tel_co = mechanician.tel_co,
            this.mechanician.boss_co = mechanician.boss_co,
            this.mechanician.address_co = mechanician.address_co,
            this.mechanician.city_co = mechanician.city_co,
            this.mechanician.website_co = mechanician.website_co,

            this.signupMechanicianWorkflowService.validateStep(STEPS_MECHANICIAN.companyDetails);
    }

    getWorkSchedules(): WorkSchedules {
        return this.mechanician.workSchedules;
    }

    setWorkSchedules(workSchedules: WorkSchedules) {

        this.isScheduleFormValid = true;

        this.mechanician.workSchedules = workSchedules;

        this.signupMechanicianWorkflowService.validateStep(STEPS_MECHANICIAN.workSchedules);
    }

    getMechanician(): Mechanician {
        return this.mechanician;
    }

    resetMechanician(): Mechanician {
        this.signupMechanicianWorkflowService.resetSteps();
        this.mechanician.clear();
        this.isLoginFormValid = this.isCompanyFormValid = this.isScheduleFormValid = false;
        return this.mechanician;
    }

    isFormValid() {
        // Return true if all forms had been validated successfully; otherwise, return false
        return this.isLoginFormValid &&
            this.isCompanyFormValid &&
            this.isCompanyFormValid;

    }

    savePDFAndCreateCompanyId(data: Mechanician): Observable<any> {
        return this.http.post(this.createCompanyUrl, data)
            .map((response => {
                const nodeResponse: NodeResponse = JSON.parse(response.json());
                return nodeResponse;
            }));
    }


    register(data: any): Observable<any> {
        return this.http.post(this.createCompanyUrl, data)
            .map((response => {
                const nodeResponse: NodeResponse = JSON.parse(response.json());
                return nodeResponse;
            }));
    }
}

