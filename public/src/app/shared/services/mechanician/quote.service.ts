import { Injectable } from '@angular/core';
import { Quote } from '../../models/mechanician/quote.model';
import { Observable } from 'rxjs/Observable';
import { NodeResponse } from '../../models/node_response';
import { Http } from '@angular/http';

@Injectable()
export class QuoteService {

  private quote: Quote = new Quote();

  baseUrl = 'http://localhost:3000/api';
  createQuoteUrl = this.baseUrl + '/quote/createQuote';
  deleteQuoteUrl = this.baseUrl + '/quote/deleteQuote';
  waitingQuoteForCompanyUrl = this.baseUrl + '/quote/getWaitingQuoteForCompany';
  quotesByAdUrl = this.baseUrl + '/quote/getQuotesByAd?id_ad=';
  acceptQuoteUrl = this.baseUrl + '/quote/acceptQuote';
  quoteAcceptedUrl = this.baseUrl + '/quote/getAdsWithAcceptedQuotes';
  quoteRefusedUrl = this.baseUrl + '/quote/getAdsWithRefusedQuotes';
  validatedQuoteByAdUrl = this.baseUrl + '/quote/getValidatedQuoteByAd?id_ad=';
  getQuoteUrl =  this.baseUrl + '/quote/getQuote?id_quote=';



  constructor(private http: Http) { }

  setQuote(quote: Quote) {

    this.quote.name_quote = quote.name_quote;
    this.quote.date_quote = quote.date_quote;
    this.quote.accept_quote = quote.accept_quote;
    this.quote.description_quote = quote.description_quote;
    this.quote.price_estimate = quote.price_estimate;
    this.quote.id_ad = quote.id_ad;
    this.quote.id_co = quote.id_co;
  }

  getQuote(): Quote {
    return this.quote;
  }

  createQuote(data: Quote): Observable<NodeResponse> {
    data.userId = JSON.parse(localStorage.getItem('token')).user;
    return this.http.post(this.createQuoteUrl, data).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

  deleteQuote(data: Quote): Observable<NodeResponse> {
    data.userId = JSON.parse(localStorage.getItem('token')).user;
    return this.http.post(this.deleteQuoteUrl, data).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

  getWaitingQuotes(): Observable<NodeResponse> {
    return this.http.get(this.waitingQuoteForCompanyUrl).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

  getQuotesByAd(id_ad): Observable<NodeResponse> {
    return this.http.get(this.quotesByAdUrl + id_ad).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

  acceptQuote(data: Quote): Observable<NodeResponse> {
    data.accept_quote = true;
    return this.http.post(this.acceptQuoteUrl, data).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

  getQuoteAccepted(): Observable<NodeResponse> {
    return this.http.get(this.quoteAcceptedUrl).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

  getQuoteRefused(): Observable<NodeResponse> {
    return this.http.get(this.quoteRefusedUrl).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }

  getValidatedQuoteByAd(id_ad): Observable<NodeResponse> {
    return this.http.get(this.validatedQuoteByAdUrl + id_ad).map(response => {
      const nodeResponse: NodeResponse = JSON.parse(response.json());
      return nodeResponse;
    });
  }
}
