import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { NodeResponse } from '../models/node_response';

@Injectable()
export class AuthenticationService {
  baseUrl = 'http://localhost:3000/api';
  loginUrl = this.baseUrl + '/signin';
  currentCarDriverUrl = this.baseUrl + '/users/getUser';
  currentMechanicianUrl = this.baseUrl + '/company/getCompany';

  constructor(private http: Http) { }

  login(email: string, password: string): Observable<NodeResponse> {
    localStorage.removeItem('token');
    return this.http.post(this.loginUrl, { email: email, password: password })
      .map((response: Response) => {
        const nodeResponse: NodeResponse = JSON.parse(response.json());

        if (nodeResponse.data) {
          // Store token in navigator
          localStorage.setItem('token', JSON.stringify(nodeResponse.data.tokenObject));
        } else {
          // TODO treat wrong login
        }
        return nodeResponse;
      });
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    localStorage.removeItem('token');
  }

  getToken() {
    return JSON.parse(localStorage.getItem('token'));
  }

  getCurrentUser(): Observable<NodeResponse> {
    const userId = JSON.parse(localStorage.getItem('token')).user;
    const userType = JSON.parse(localStorage.getItem('token')).userType;
    if (userType === 'driver') {
      return this.http.get(this.currentCarDriverUrl + '?id=' + userId).map(response => {
        const nodeResponse: NodeResponse = JSON.parse(response.json());
        return nodeResponse;
      });
    } else if (userType === 'company') {
      return this.http.get(this.currentMechanicianUrl + '?id=' + userId).map(response => {
        const nodeResponse: NodeResponse = JSON.parse(response.json());
        return nodeResponse;
      });
    }
  }
}
