import { SignupMechanicianModule } from './signup-mechanician.module';

describe('SignupMechanicianModule', () => {
  let signupMechanicianModule: SignupMechanicianModule;

  beforeEach(() => {
    signupMechanicianModule = new SignupMechanicianModule();
  });

  it('should create an instance', () => {
    expect(signupMechanicianModule).toBeTruthy();
  });
});
