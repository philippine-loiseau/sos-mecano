import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeRoutingModule } from './home/home-routing.module';
import { LoginRoutingModule } from './login/login-routing.module';


const routes: Routes = [
  { path: '**', redirectTo: 'home'},
];

@NgModule({
  imports: [RouterModule.forChild(routes), HomeRoutingModule, LoginRoutingModule],
  exports: [RouterModule],
})

export class AppRoutingModule { }
