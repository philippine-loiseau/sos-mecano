import { MechanicianModule } from './mechanician.module';

describe('MechanicianModule', () => {
  let mechanicianModule: MechanicianModule;

  beforeEach(() => {
    mechanicianModule = new MechanicianModule();
  });

  it('should create an instance', () => {
    expect(mechanicianModule).toBeTruthy();
  });
});
