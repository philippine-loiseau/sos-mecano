export interface NodeResponse {
    type: string;
    data: any;
    success: boolean;
}
