import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from '../../shared/services/authentification.service';
import { User } from '../../shared/models/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  constructor(private authentificationService: AuthentificationService) { }

  ngOnInit() {
  }
  
}
