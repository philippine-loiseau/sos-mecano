import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AuthentificationService } from './services/authentification.service';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [AuthentificationService],
  declarations: []
})
export class SharedModule { }
