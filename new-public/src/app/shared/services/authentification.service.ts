import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { User } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import { $ } from 'protractor';

@Injectable()

export class AuthentificationService {

  constructor(private http: HttpClient) { }

  login(user: User): Observable<User> {
    return this.http.post<User>( environment.baseUrl + '/signin', { user });
  }

}