import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})

export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  
  toggleSection() {
    $(function () {
      $('.footer-links-holder h3').click(function () {
        $(this).parent().toggleClass('active');
      });
    });
  }

}
