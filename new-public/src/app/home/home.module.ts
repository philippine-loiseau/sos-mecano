import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { LayoutModule } from '../layout/layout.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

import { HomeHeaderComponent } from './home/home-header/home-header.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    FontAwesomeModule,
    RouterModule,
    BsDropdownModule.forRoot()
  ],
  declarations: [HomeComponent, HomeHeaderComponent]
})
export class HomeModule { } 
