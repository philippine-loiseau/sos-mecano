import { Component, OnInit, Input } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home-header',
  templateUrl: './home-header.component.html',
  styleUrls: ['./home-header.component.scss']
})
export class HomeHeaderComponent implements OnInit {

  @Input() iconSite: string;
  @Input() title: string;
  @Input() navLink: string;
  @Input() iconLink: string;
  @Input() routerLink: string;
  
  faBars = faBars;

  constructor() { }

  ngOnInit() {
  }

}
