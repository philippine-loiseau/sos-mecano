import { Component, OnInit } from '@angular/core';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  title: string = 'SOS MECANO';
  navLink: string = 'Se connecter';
  faUserCircle = faUserCircle;
  iconSite: string = './../../../../assets/img/car-repair.png';
  routerLink: string = '/login';

  constructor() { }

  ngOnInit() {
  }

}
